<?php  
	
	include("Function.php");

	// ------------------------ Login admin authentication-------------------------------------
	if(isset($_POST['admin']) && isset($_POST['password'])){
		
		// ReCAPTCHA 
		$secretKey = "6Ld8hIYUAAAAAJPRjsAQUOFwU6S0LhllIs0j-Vuz";
		$responseKey = $_POST["g-recaptcha-response"];
		$userIP = $_SERVER['REMOTE_ADDR'];
		$url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
		$response = file_get_contents($url);
		$response = json_decode($response);
		if($response->success){
			//check if password & admin exist and match
			$user = $_POST['admin'];
			$passwordLogin = $_POST['password'];
			$admin = checkAdminLogin("protected/AdminAccount.txt",$user); // return a string or ""
			$checkLogin = allowUserConnect("protected/AdminAccount.txt.txt",$passwordLogin,$admin);

			if ($checkLogin == 1){ 	// We let him connect if checkLogin is 1
				session_start();
				$_SESSION['id'] = 1;
				$_SESSION['connect'] ="active";
				$_SESSION['userType'] ="admin";
				$_SESSION['username'] = $_POST['admin'];

				$_SESSION['home'] = "home/";
				$_SESSION['home/username/etc']=NULL;
				$_SESSION['home/username'] = $_SESSION['home'].$_SESSION['username'].$_SESSION['home/username/etc'];
				header('location:HomeAdmin.php');
			}
			else {
				header('location:loginAdmin.php');	
			}
		}else{
			header('location:loginAdmin.php');	
		}

			
	}

	// ------------------------ Signup authentication -----------------------------
	else if (isset($_POST['FirstName']) && isset($_POST['LastName']) && isset($_POST['email']) && isset($_POST['UserName']) && isset($_POST['Password1']) && isset($_POST['Password2'])){
		
		//$_SESSION['connect'] ="active";
		
		//AZERTYuiop1234567
		// 1st condition : check username if exist already in your FILE
		$UsernameStatus = NULL;
		$UsernameStatus = checkUsernameSignUp("protected/AdminAccount.txt",$_POST['UserName']); // the value returned is NULL: username don't exist | create account or "error": Username exist | ask another username
		$pwdSecure =NULL;
		echo $UsernameStatus;
		if($UsernameStatus != NULL)	
			$pwdSecure = "'".$UsernameStatus."' exist already! Change your username please!";

		
		// 2nd condition : check if password is secure
		$pwdSecure1 = checkPasswordSecure($_POST['Password1']); //return blank:ok or error: not ok
		$pwdSecure = $pwdSecure.$pwdSecure1;
		// 3rd condition : check if password1 = password2
		$pwdEqual = "";
		$pwdEqual = checkPasswordSignUp($_POST['Password1'], $_POST['Password2']); //return 1: ok or 2: not ok

		$pwdSecure = $pwdSecure.$pwdEqual;
		
		// If everything is allright we create the account in the file
		if ($pwdSecure == NULL){ //everything ok we create the account
			$account = "";
			$username = $_POST['UserName'];
			$password = hashPwd($_POST['Password1'], $username);
			$email = $_POST['email'];
			$LastName = $_POST['LastName'];
			$FirstName = $_POST['FirstName'];

			$account = $username." ".$password." ".$email." ".$LastName." ".$FirstName.PHP_EOL;
			$fileName = "protected/AdminAccount.txt";
			// write the account in the file
			$content = file_get_contents($fileName);
			$content = $content.$account;
			echo $content;
			file_put_contents($fileName, $content);
		}

		header('location:SignupAdmin.php?error='.$pwdSecure);	
	}
	else{
		header('location:SignupAdmin.php');
	}
?>