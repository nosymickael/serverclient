<?php 
	//----------Function for admin validating user account (3 functions)------------- 
	
	// function 1: to read and copy the account line  that has been requested
	function readRequestUserAccount($file,$usernameToCheck){
		$handle = fopen($file, "r");
		$value = "";
		if ($handle) {
		    while (($line = fgets($handle)) !== false) {
		        // process the line read.
		        $word = explode(" ", $line);
		        if($usernameToCheck == $word[0]){
		        	$value = $line;
		        	
		        	return $value;
		        }
	    	}
	    	fclose($handle);
		}
		return $value; //if value return "" (blank) the username is wrong - if value return something username exist
	}
	

	// function 2: to delete the account line after the admin accepted => it last another function to write the account accepted into AccountAccepted.php and to create a folder for the user
	function deletePreAcceptUser($fileName, $username){
		$AccountLine = readRequestUserAccount($fileName,$username);
		$contents = file_get_contents($fileName);
		$contents = str_replace($AccountLine, '', $contents);
		file_put_contents($fileName, $contents);
	}

	// function to write the account accepted & create a folder for the user inside Home
	function createAcceptedAccountFolder($filename, $username, $account){
		createFolder('Home/'.$username);
	}

	// function to check login
	function checkAdminLogin($file,$usernameToCheck){
		$handle = fopen($file, "r");
		$value = "";
		if ($handle) {
		    while (($line = fgets($handle)) !== false) {
		        // process the line read.
		        $word = explode(" ", $line);
		        if($usernameToCheck == $word[0]){
		        	$value = $line;
		        	return $value;
		        }
	    	}
	    	fclose($handle);
		}
		return $value; //if value return "" (blank) the username is wrong - if value return something username exist
	}

	// List all User
		
	function listAllUserAccepted($username){
		echo "
			<table>
			<form method ='post' action='FileProcess.php' enctype='multipart/form-data'>
			
		";
		$dir = 'Home/'.$username;
		$handle = opendir($dir);
		echo '
			<tr>
				<td><input type="file" name="file" ></td>
				<td><input type="submit" name="Upload" value ="Upload"></td>
				<td></td>
				<td><input type="submit" name="NewFolder" value ="New folder"></td>
			</tr>
			</form>
			';
		//$fileNumber = countFolder($handle);
		if ($handle){
			$i =0;
		    while (false !== ($entry = readdir($handle))){

		        if ($entry != "." && $entry != "..") {

		            echo '
					<form method ="post" action="FileProcess.php" enctype="multipart/form-data">
						<tr>';
							
					if(isFolder($entry)){
						echo '<td><b>'.$entry.'</b></td>';
						$_SESSION[$i. " dir"]= $entry;
						echo '
							<td><button type="submit" name="EnterDir" value ="'.$i.' Enter">Enter</button></td>
							<td><button type="submit" name="RenameDir" value ="'.$i.' Rename">Rename</button></td>
							<td><button type="submit" name="DeleteDir" value ="'.$i.' Delete">Delete</button></td>
							<td><button type="submit" name="DownloadDir" value ="'.$i.' Download">Download</button></td>
						</tr>
		            	';
		        	}else{
		        		echo '<td>'.$entry.'</td>';
		        		$_SESSION[$i." file"]= $entry;

		        		echo '
							<td><button type="submit" name="Modify" value ="'.$i.' Modify">Modify</button></td>
							<td><button type="submit" name="Rename" value ="'.$i.' Rename">Rename</button></td>
							<td><button type="submit" name="Delete" value ="'.$i.' Delete">Delete</button></td>
							<td><button type="submit" name="Download" value ="'.$i.' Download">Download</button></td>
						</tr>
		            ';
		        	}
		        }
		        $i++;
		    }
	    	closedir($handle);
		}
		echo "
			</form>
			</table>
		";
	}

	// list all user for management (admin) 
	function listUserAccepted($fileName){
		$handle = fopen($fileName, "r");
		$value = array();
		if ($handle){
			$i = 0;
			echo '
					<form method="post" action="UserManagement.php" enctype="multipart/form-data">
					<table>
				';
			$i = 0;
		    while (($line = fgets($handle)) !== false) {
		        // process the line read.
		    
		        $user = explode(" ", $line);
		    $_SESSION[$i." userAccepted"] = $user[0];
		        echo '
		        	<tr>
		        		<td>'.$user[0].'</br></td>
		        		<td><button type="submit" name ="Delete" value="'.$i.'" >Delete</button></td>
		        		<td><button type="submit" name ="ChangePassword" value="'.$i.'" >Change password</button></td>
		        	</tr>
		        ';
	    		$i++;
	    	}
	    	echo '
					</form>
					</table>
				';
	    	fclose($handle);
		}
	}

	// list user requested (admin)
	function listUserRequested($fileName){
		$handle = fopen($fileName, "r");
		$value = array();
		if ($handle){
			$i = 0;
			echo '
					<form method="post" action="UserManagement.php" enctype="multipart/form-data">
					<table>
				';
			$i =0;
		    while (($line = fgets($handle)) !== false) {
		        // process the line read.
		        $user = explode(" ", $line);
		        $_SESSION[$i.' user'] = $user[0];
		        echo '
		        	<tr>
		        		<td>'.$user[0].'</br></td>
		        		<td><button type="submit" name ="Accept" value="'.$i.'" >Accept</button></td>
		        		<td><button type="submit" name ="Refuse" value="'.$i.'" >Refuse</button></td>
		        	</tr>
		        ';
		        $i++;
	    	}
	    	echo '
					</form>
					</table>
				';
	    	fclose($handle);
		}
	}
	//-------------------------Function for home.php-------------------------------------- 
		//||||||| create a file
		function createFile($fileName){
			$myfile = fopen($fileName, "w");
			fclose($myfile);
		}
		
		//||||||| write in a file
		function writeFile($fileName,$value){
			$myfile = fopen($fileName, "w") or die("Unable to open file!");
			fwrite($myfile, $value);
			fclose($myfile);
		}

		//||||||| Delete a file
		function deleteFile($fileName){
			$myFile = $fileName;
			unlink($myFile) or die("Couldn't delete file");
		}
		
		//||||||| Read a file | return the value of the file
		function readMyFile($fileName){
			$myfile = fopen($fileName, "r") or die("Unable to open file!");
			$value = fread($myfile,filesize($fileName));
			fclose($myfile);
			return $value;
		}
		
		//||||||| Upload file
		function uploadFile($path, $uploaded_file){
			if(!empty($_FILES[$uploaded_file])){
			    $path = $path . basename( $_FILES[$uploaded_file]['name']);
			    
			    if(move_uploaded_file($_FILES[$uploaded_file]['tmp_name'], $path)) {
			      echo "The file ".  basename( $_FILES[$uploaded_file]['name']). 
			      " has been uploaded";
			    }else{
			        echo "There was an error uploading the file, please try again!";
			    }
			}else{
				echo "File not found";
			}
		}


		//||||||||||| Create zip
		function createZip($zip,$dir){
		 if (is_dir($dir)){

		  if ($dh = opendir($dir)){
		   while (($file = readdir($dh)) !== false){
		 
		    // If file
		    if (is_file($dir.$file)) {
		     if($file != '' && $file != '.' && $file != '..'){
		 
		      $zip->addFile($dir.$file);
		     }
		    }else{
		     // If directory
		     if(is_dir($dir.$file) ){

		      if($file != '' && $file != '.' && $file != '..'){

		       // Add empty directory
		       $zip->addEmptyDir($dir.$file);

		       $folder = $dir.$file.'/';
		 
		       // Read data of the folder
		       createZip($zip,$folder);
		      }
		     }
		 
		    }
		 
		   }
		   closedir($dh);
		  }
		 }
		}

		//||||||| Download any document from server function
		function downloadFile($file){
			if (file_exists($file)) {
			    header('Content-Description: File Transfer');
			    header('Content-Type: application/octet-stream');
			    header('Content-Disposition: attachment; filename="'.basename($file).'"');
			    header('Expires: 0');
			    header('Cache-Control: must-revalidate');
			    header('Pragma: public');
			    header('Content-Length: ' . filesize($file));
			    readfile($file);
			    exit;
			}
		}

		//||||||| Create a folder or directory for user with mode (rules) 
		function createFolder($folderName){
			if (!is_dir($folderName)) {
    			mkdir($folderName);         
				echo "'".$folderName ."' folder is created!";
			}
			else{
				echo "'".$folderName ."' exist already!";
			}
		}

		//||||||| Delete a folder
		function deleteFolder($folderName){
		    //if(is_dir($folderName)){
		        $files = glob($folderName . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

		        foreach($files as $file){
		            deleteFolder($file);      
		        }
		        if(is_dir($folderName))
		        	rmdir($folderName);
		    echo "'".$folderName ."' folder is deleted!<br>";
		    //} elseif(is_file($folderName)) {
		    //    unlink($folderName);  
		    //}
		}

		// Check wheither dir or file

	function isFolder($string){
		$isFolder = true;
		$str_array = str_split($string);
		$counter = 0;
		while($counter < count($str_array))
		{
		    if($str_array[$counter] == '.') {
		        $isFolder = false;
		        break;
		    } else {
		        $counter++;
		    }
		}
		return $isFolder;
	}
	function countFolder($dir) {
	 return (count(scandir($dir)) - 2);
	}

		// List all file in a dir
		
	function listAllFile($username){
		
		$_SESSION['home'] = "home/";
		$_SESSION['home/username/etc']=NULL;
		$_SESSION['home/username'] = $_SESSION['home'].$username.$_SESSION['home/username/etc'];
		$dir = $_SESSION['home/username'];
		echo "
			<table>
			<form method ='post' action='FileProcess.php' enctype='multipart/form-data'>
			
		";
		
		$handle = opendir($dir);
		echo '
			<tr>
				<td><input type="file" name="file" ></td>
				<td><input type="submit" name="Upload" value ="Upload"></td>
				<td></td>
				<td><input type="submit" name="NewFolder" value ="New folder"></td>
			</tr>
			</form>
			';
		//$fileNumber = countFolder($handle);
		if ($handle){
			$i =0;
		    while (false !== ($entry = readdir($handle))){

		        if ($entry != "." && $entry != "..") {

		            echo '
					<form method ="post" action="FileProcess.php" enctype="multipart/form-data">
						<tr>';
							
					if(isFolder($entry)){
						echo '<td><b>'.$entry.'</b></td>';
						$_SESSION[$i. " dir"]= $entry;
						echo '
							<td><button type="submit" name="EnterDir" value ="'.$i.' Enter">Enter</button></td>
							<td><button type="submit" name="RenameDir" value ="'.$i.' Rename">Rename</button></td>
							<td><button type="submit" name="DeleteDir" value ="'.$i.' Delete">Delete</button></td>
							<td><button type="submit" name="DownloadDir" value ="'.$i.' Download">Download</button></td>
						</tr>
		            	';
		        	}else{
		        		echo '<td>'.$entry.'</td>';
		        		$_SESSION[$i." file"]= $entry;

		        		echo '
							<td><button type="submit" name="Modify" value ="'.$i.' Modify">Modify</button></td>
							<td><button type="submit" name="Rename" value ="'.$i.' Rename">Rename</button></td>
							<td><button type="submit" name="Delete" value ="'.$i.' Delete">Delete</button></td>
							<td><button type="submit" name="Download" value ="'.$i.' Download">Download</button></td>
						</tr>
		            ';
		        	}
		        }
		        $i++;
		    }
	    	closedir($handle);
		}
		echo "
			</form>
			</table>
		";
	}	
	
	//-------------------------Functions for login.php------------------------------- 
	
		////////// check username if exist in the file
	function checkUsernameLogin($file,$usernameToCheck){
		$handle = fopen($file, "r");
		$value = "";
		if ($handle) {
		    while (($line = fgets($handle)) !== false) {
		        // process the line read.
		        $word = explode(" ", $line);
		        if($usernameToCheck == $word[0]){
		        	$value = $line;
		        	return $value;
		        }
	    	}
	    	fclose($handle);
		}
		return $value; //if value return "" (blank) the username is wrong - if value return something username exist
	}

		///////////// password dehash to compare with what the user input
				// Use explode before using this
	function passwordDehash($username, $password, $passwordHashed){
		$hashedU = hash('sha256', $username);
		$check = $password.$hashedU;
		if (password_verify($check, $passwordHashed)) {
	    	return 1; // return 1 if it is valid
		} else {
		    return 2; // return 2 if it is not valid
		}
	}

		////////// last function before letting the user log in
	function allowUserConnect($fileName, $password, $account){
		if($account != ""){
			$Account = explode(" ", $account);
			$dehash = passwordDehash($Account[0], $password, $Account[1]); // return 1 if ok and 2 if not ok
		}
		else
			$dehash = 2;
		return $dehash; // 1 if ok and 2 if not ok
	}
	
	//-------------------------Functions for Signup.php----------------------------- 
	

	// ||||||| function to check if the username exist already
	function checkUsernameSignUp($file,$usernameToCheck){
		$handle = @fopen($file, "r");
		$i = 0;
		$ok = NULL;
		// check the username
		if ($handle) {
		    while (!feof($handle)) {
		        $buffer = fgets($handle); // fgets: Function to get a line in a file
		        $exploded_data = explode(" ",$buffer); // explode take word per word and put it into an array which is $exploded_data
		        
		        if ($usernameToCheck == $exploded_data[0]){ //check if the username exist already
		        	$ok = $exploded_data[0];
		        	echo "Username already exist, please choose another username!<br>";
		 			break;
		        }
		    	$i++; // number of account = $i-1
		    }
		    fclose($handle);
		}
		return $ok; // if $ok equal NULL => username does not exist
	}



	// ||||||| function: check if password is secure enough
	function checkPasswordSecure($pwd) {
    $errors = "";

    if (strlen($pwd) < 8) {
        $errors = $errors . "Password too short!<br>";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors= $errors . "Password must include at least one number!<br>";
    }

    if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $errors= $errors . "Password must include at least one letter!<br>";
    }     

    	return $errors;
	}	


	// ||||||| function: check if the password are equal
	function checkPasswordSignUp($pwd1, $pwd2){
		if($pwd1 == $pwd2){
			return "";
		}
		else{
			return "Your password is not equal retype again!";
		}
	}

	// hashing the password before saving into the file
	function hashPwd($password, $username){
		$salt = hash("sha256", $username);
		$passwordToHash = $password.$salt;
		$value = password_hash($passwordToHash, PASSWORD_BCRYPT);	
		return $value;
	}
?>