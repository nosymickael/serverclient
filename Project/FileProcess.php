<?php  
	session_start();
	include('Function.php');

		// UPLOAD
	if(isset($_POST['Upload']) && isset($_FILES['file'])){ // UPLOAD FILE
		uploadFile("Home/".$_SESSION['username']."/", "file");
		echo "upload ok";
		if($_SESSION['userType'] =="admin"){
			header('location:HomeAdmin.php');
		}elseif($_SESSION['userType'] == "user"){
			header('location:Home.php');
		}

		
	}elseif(isset($_POST['NewFolder'])){ // NEW FOLDER
		$pieces = explode(" ", $_POST['NewFolder']);
		if($_SESSION['userType'] =="admin"){
			echo '
			<form action="HomeAdmin.php" method="post" enctype="multipart/form-data">';
		}elseif($_SESSION['userType'] == "user"){
			echo '
			<form action="Home.php" method="post" enctype="multipart/form-data">';
		}
		echo '<p> <u> New folder </u> : </p>
			<input type="text" name="textArea" value ="">';
		$username = $_SESSION['username'];

		echo '
			<p>
			    <button type="submit" name ="saveNewFolder" value="Save" />Save</button>
			    <button type="submit" name ="cancelNewFolder" value="Cancel" />Cancel</button>
			</p>
			</form>
			';
		

	}elseif(isset($_POST['RenameDir'])){ // RENAME FOLDER
		$pieces = explode(" ", $_POST['RenameDir']);
		$filename = $_SESSION[$pieces[0]." dir"];
		
		if($_SESSION['userType'] =="admin"){
			echo '
			<form action="HomeAdmin.php" method="post" enctype="multipart/form-data">';
		}elseif($_SESSION['userType'] == "user"){
			echo '
			<form action="Home.php" method="post" enctype="multipart/form-data">';
		}
		
		echo '	<p> <u> File name </u> : '.$filename.'</p>
			<input type="text" name="textArea" value ="'.$filename.'">';
		$username = $_SESSION['username'];
		$file = "Home/".$username."/".$filename;
		
		
		
		echo '
			<p>
				<td><input type="hidden" name="FileName" value ="'.$filename.'"></td>
			    <button type="submit" name ="saveRename" value="Save" />Save</button>
			    <button type="submit" name ="cancelRename" value="Cancel" />Cancel</button>
			</p>
			</form>
			';
		

	}elseif(isset($_POST['DeleteDir'])){ // DELETE FOLDER
		$pieces = explode(" ", $_POST['DeleteDir']);
		$filename = $_SESSION[$pieces[0]." dir"];
		$username = $_SESSION['username'];
		$folderName = "Home/".$username."/".$filename;
		deleteFolder($folderName);
		
		if($_SESSION['userType'] =="admin"){
			header('location:HomeAdmin.php');
		}elseif($_SESSION['userType'] == "user"){
			header('location:Home.php');
		}
	}elseif(isset($_POST['Modify'])){ // MODIFY FILE
		$pieces = explode(" ", $_POST['Modify']);
		$filename = $_SESSION[$pieces[0]." file"];
		
		if($_SESSION['userType'] =="admin"){
			echo '
			<form action="HomeAdmin.php" method="post" enctype="multipart/form-data">';
		}elseif($_SESSION['userType'] == "user"){
			echo '
			<form action="Home.php" method="post" enctype="multipart/form-data">';
		}

		echo '	<p> <u> File name </u> : '.$filename.'</p>
			<TEXTAREA name="textArea" rows=4 cols=40>';
		$username = $_SESSION['username'];
		$file = "Home/".$username."/".$filename;
		//echo $file;
		$content = file_get_contents($file);
		
		echo $content.
		 '</TEXTAREA>
			<p>
				<td><input type="hidden" name="FileName" value ="'.$filename.'"></td>
			    <button type="submit" name ="saveModify" value="Save" />Save</button>
			    <button type="submit" name ="cancelModify" value="Cancel" />Cancel</button>
			</p>
			</form>
			';
	}elseif(isset($_POST['Rename'])){ // RENAME FILE
		$pieces = explode(" ", $_POST['Rename']);
		$filename = $_SESSION[$pieces[0]." file"];
		
		if($_SESSION['userType'] =="admin"){
			echo '
			<form action="HomeAdmin.php" method="post" enctype="multipart/form-data">';
		}elseif($_SESSION['userType'] == "user"){
			echo '
			<form action="Home.php" method="post" enctype="multipart/form-data">';
		}


		echo	'<p> <u> File name </u> : '.$filename.'</p>
			<input type="text" name="textArea" value ="'.$filename.'">';
		$username = $_SESSION['username'];
		$file = "Home/".$username."/".$filename;
		
		
		
		echo '
			<p>
				<td><input type="hidden" name="FileName" value ="'.$filename.'"></td>
			    <button type="submit" name ="saveRename" value="Save" />Save</button>
			    <button type="submit" name ="cancelRename" value="Cancel" />Cancel</button>
			</p>
			</form>
			';
		

	}elseif(isset($_POST['Delete'])){ // DELETE FILE
		$pieces = explode(" ", $_POST['Delete']);
		$filename = $_SESSION[$pieces[0]." file"];
		$username = $_SESSION['username'];
		$file = "Home/".$username."/".$filename;
		if(file_exists($file)){ 
			deleteFile($file);	        
		}
		
			if($_SESSION['userType'] =="admin"){
				header('location:HomeAdmin.php');
			}elseif($_SESSION['userType'] == "user"){
				header('location:Home.php');
			}header('location:Home.php');
	}elseif(isset($_POST['Download'])){ // DOWNLOAD FILE
		$pieces = explode(" ", $_POST['Download']);
		$filename = $_SESSION[$pieces[0]." file"];
		$username = $_SESSION['username'];
		$file = "Home/".$username."/".$filename;
		if(file_exists($file)){ 
			downloadFile($file);
			if($_SESSION['userType'] =="admin"){
				header('location:HomeAdmin.php');
			}elseif($_SESSION['userType'] == "user"){
				header('location:Home.php');
			}
		}
	}elseif(isset($_POST["EnterDir"])){ // ENTER DIR
		$pieces = explode(" ", $_POST['EnterDir']);
		$file = $_SESSION[$pieces[0]." dir"];
		$username = $_SESSION['username'];
		//echo $file;
		$_SESSION['home/username/etc'] = "/".$file;
		if($_SESSION['userType'] =="admin"){
				header('location:HomeAdmin.php?dir='.$_SESSION["home/username/etc"]);
			}elseif($_SESSION['userType'] == "user"){
				header('location:Home.php');
			}
	}elseif(isset($_POST['DownloadDir'])){ // DOWNLOAD DIR
		// Create ZIP file
		$pieces = explode(" ", $_POST['DownloadDir']);
		$file = $_SESSION[$pieces[0]." dir"];
		$username = $_SESSION['username'];

			 $zip = new ZipArchive();
			 $filename = "Home/".$username."/".$file.".zip";

		 	if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
		  		echo "Cannot open zip";
		  		exit("cannot open <$filename>\n");
		 	}

			$dir = "Home/".$username."/";

			 // Create zip
		 	createZip($zip,$dir);

		 	$zip->close();
		

		

		// Download Created Zip file
		if(isset($_POST['DownloadDir'])){
			$pieces = explode(" ", $_POST['DownloadDir']);
			$file = $_SESSION[$pieces[0]." dir"];
			$username = $_SESSION['username'];
			$filename = "Home/".$username."/".$file.".zip";

			if (file_exists($filename)) {
				header('Content-Type: application/zip');
				header('Content-Disposition: attachment; filename="'.basename($filename).'"');
				header('Content-Length: ' . filesize($filename));

				flush();
				readfile($filename);
				  // delete file
				unlink($filename);
		 
			}
			if($_SESSION['userType'] =="admin"){
				header('location:HomeAdmin.php');
			}elseif($_SESSION['userType'] == "user"){
				header('location:Home.php');
		}
		}
	}else{
		if($_SESSION['userType'] =="admin"){
			header('location:HomeAdmin.php');
		}elseif($_SESSION['userType'] == "user"){
			header('location:Home.php');
		} 
	}
?>